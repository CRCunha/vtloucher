import React from 'react';
import Router from './routes';
import './app.css';
import { BrowserRouter } from 'react-router-dom';

//http://cristianapp.com.br.s3-website-sa-east-1.amazonaws.com/

const App = () => {
  return (
    <React.StrictMode>
      <BrowserRouter>
        <div className="App">
          <Router>
            <Router />
          </Router>
        </div>
        <div className="AppNone"></div>
      </BrowserRouter>
    </React.StrictMode>
  );
};

export default App;
