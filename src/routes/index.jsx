import React from 'react';
import { Routes, Route } from 'react-router-dom';
import '../App';

import Lazy from '../components/lazyPage';
// Pages
let Vt = React.lazy(() => import('../pages/vt/index'));

const Router = () => {
  return (
    <div>
      <React.Suspense fallback={<Lazy />}>
        <Routes>
          <Route path="/" element={<Vt />} />
        </Routes>
      </React.Suspense>
    </div>
  );
};

export default Router;
