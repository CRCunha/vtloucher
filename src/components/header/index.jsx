import useStyles from './styles';
import LogoImage from '../../assets/images/logoSimple.png';
import buttonHeader from '../../assets/images/buttonHeader.png';

import { motion } from 'framer-motion';
import React from 'react';

const Header = () => {
  const classes = useStyles();
  return (
    <div className={classes.header}>
      <div className={classes.headerContainer}>
        <img src={LogoImage} alt="logo" />

        <motion.div whileTap={{ scale: 0.95 }}>
          <a
            href="samp://149.56.181.16:7777"
            className={classes.buttonContainer}
          >
            <img src={buttonHeader} alt="Button" />
          </a>
        </motion.div>
      </div>
    </div>
  );
};

export default Header;
