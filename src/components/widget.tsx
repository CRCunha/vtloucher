import { ChatTeardropDots } from 'phosphor-react';
import { Popover } from '@headlessui/react';

const Widget = () => {
  return (
    <Popover className="absolute bottom-5 right-5">
      <Popover.Panel>test</Popover.Panel>
      <Popover.Button className="bg-brand-500 rounded-full p-3 text-white flex items-center group hover:bg-brand-600 transition-colors">
        <ChatTeardropDots className="w6 h6" size={28} />
        <span className="max-w-0 overflow-hidden group-hover:max-w-xs transition-all duration-500 ease-linear">
          <span className="pl-2">Feedback</span>
        </span>
      </Popover.Button>
    </Popover>
  );
};

export default Widget;
