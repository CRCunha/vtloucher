import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  comandsBarMain: {
    width: '100vw',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  content: {
    height: 28,
    width: '100%',
    backgroundColor: '#f9f9f9',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
    borderTop: 'solid 1px #f5f5f5',
  },
});

export default useStyles;
