import React from 'react';
import useStyles from './styles';

const ComandsBar = () => {
  const classes = useStyles();
  return (
    <div className={classes.comandsBarMain}>
      <div className={classes.content}></div>
    </div>
  );
};

export default ComandsBar;
