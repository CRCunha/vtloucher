import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  lazyContainer: {
    height: '50vh',
    width: '90vw',
    backgroundColor: '#fff',

    padding: '50vh 5vw 0 5vw',
  },
});

export default useStyles;
