import React from 'react';
import LinearProgress from '@mui/material/LinearProgress';
import useStyles from './styles';

const Lazy = () => {
  const classes = useStyles();
  return (
    <div className={classes.lazyContainer}>
      <LinearProgress fullWidth color="success" />
    </div>
  );
};

export default Lazy;
