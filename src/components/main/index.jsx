import useStyles from './styles.jsx';
//https://cdn.dribbble.com/users/1919332/screenshots/8768113/media/cf6fd068965e228567c3c7c7fab7f085.jpg
import React from 'react';
import { useState } from 'react';

import { motion } from 'framer-motion';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMessage } from '@fortawesome/free-solid-svg-icons';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  Button,
} from '@mui/material';

import logoFull from '../../assets/images/logoFull.png';
import buttonMain from '../../assets/images/buttonMain.png';
import smallButton from '../../assets/images/smallButton.png';
import buttonForm from '../../assets/images/buttoForm.png';

const SimpleDialog = (props) => {
  const { onClose, selectedValue, open } = props;
  const classes = useStyles();
  const handleClose = () => {
    onClose(selectedValue);
  };

  return (
    <Dialog fullWidth onClose={handleClose} open={open}>
      <DialogTitle>Form Spree</DialogTitle>
      <DialogContent>
        <form
          className={classes.formContainer}
          action="https://formspree.io/f/xnqrdwkj"
          method="POST"
        >
          <TextField
            className={classes.input}
            label="email"
            fullWidth
            type="email"
            name="email"
          />
          <TextField
            className={classes.input}
            label="message"
            fullWidth
            name="message"
          />
          <Button fullWidth className={classes.sendButton} type="submit">
            <img src={buttonForm} alt="Buttom" />
          </Button>
        </form>
      </DialogContent>
    </Dialog>
  );
};

const Main = () => {
  const [open, setOpen] = useState(false);
  const classes = useStyles();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (value: string) => {
    setOpen(false);
  };

  return (
    <div className={classes.main}>
      <div className={classes.mainContainer}>
        <div className={classes.logoContainer}>
          <img src={logoFull} alt="Logo Full Name" />
          <div className={classes.logoTextContainer}>
            <span>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat,
              pariatur a!
            </span>
          </div>
        </div>
        <div className={classes.connectButtonContainer}>
          <div className={classes.connectButton}>
            <img src={buttonMain} alt="Buttom" />
          </div>
          <motion.div whileTap={{ scale: 0.95 }}>
            <a href="samp://149.56.181.16:7777" className={classes.smallButton}>
              <img src={smallButton} alt="Logo Full Name" />
            </a>
          </motion.div>
        </div>
        <div className={classes.contactButtonContainer}>
          <motion.div whileTap={{ scale: 0.95 }}>
            <Button
              className={classes.contactButton}
              onClick={handleClickOpen}
              type="button"
            >
              Form Spree
              <FontAwesomeIcon className={classes.icon} icon={faMessage} />
            </Button>
          </motion.div>
        </div>
      </div>
      <SimpleDialog open={open} onClose={handleClose} />
    </div>
  );
};

export default Main;
