import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  cardMain: {
    width: 400,
    height: '85%',
    backgroundColor: '#f9f9f9',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginLeft: 10,
    borderRadius: 5,
  },
  topInfos: {
    paddingTop: 20,
    width: '90%',
    display: 'flex',
    justifyContent: 'space-between',
  },
  tittle: {
    width: '70%',
    fontSize: 14,
    fontWeight: 500,
    color: '#303030',
    textAlign: 'left',
  },
  tag: {
    height: 20,
    width: '20%',
    padding: 5,
    fontSize: 12,
    borderRadius: 20,
    fontWeight: 500,
    backgroundColor: '#FF9671',
  },
  tagFinish: {
    height: 20,
    width: '20%',
    padding: 5,
    fontSize: 12,
    borderRadius: 20,
    fontWeight: 500,
    backgroundColor: '#78C664',
  },
  bottonInfos: {
    width: '100%',
    height: '48%',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    '& img': {
      width: '30%',
    },
  },
  bar: {
    width: '100%',
    height: '2%',
  },
  data: {
    display: 'flex',
    justifyContent: 'flex-end',
    width: '85%',
    fontSize: 12,
    fontWeight: 500,
  },
});

export default useStyles;
