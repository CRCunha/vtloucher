import React, { useState } from 'react';
import useStyles from './styles';
import LinearProgress from '@mui/material/LinearProgress';

const CardAtt = ({ tag, title, link, data } = props) => {
  const classes = useStyles();
  const [progress, setProgress] = useState(0);
  const [buffer, setBuffer] = useState(10);

  const progressRef = React.useRef(() => {});
  React.useEffect(() => {
    progressRef.current = () => {
      if (progress > 100) {
        setProgress(0);
        setBuffer(10);
      } else {
        const diff = Math.random() * 10;
        const diff2 = Math.random() * 10;
        setProgress(progress + diff);
        setBuffer(progress + diff + diff2);
      }
    };
  });

  React.useEffect(() => {
    const timer = setInterval(() => {
      progressRef.current();
    }, 500);

    return () => {
      clearInterval(timer);
    };
  }, []);

  return (
    <div className={classes.cardMain}>
      <div className={classes.topInfos}>
        <div className={classes.tittle}>{title}</div>
        {tag === 'Finalizado' ? (
          <div className={classes.tagFinish}>{tag}</div>
        ) : (
          <div className={classes.tag}>{tag}</div>
        )}
      </div>
      <div className={classes.data}>{data}</div>
      <div className={classes.bottonInfos}>
        <img
          src="https://cdn-images-1.medium.com/max/1200/1*nAZ3sppQFscZyKNX5aIb-A.png"
          alt="KOTH Logo"
        />
      </div>
      <div className={classes.bar}>
        <LinearProgress fullWidth color="success" />
      </div>
    </div>
  );
};

export default CardAtt;
