import { makeStyles } from '@material-ui/styles';
import backgroundImage from '../../assets/images/background.jpg';

const useStyles = makeStyles({
  main: {
    height: '100vh',
    width: '100vw',
  },
  mainTop: {
    width: '100%',
    height: 538,
    backgroundImage: `url(${backgroundImage})`,
    backgroundSize: 'cover',
    backgroundPositionY: 'center',
    backgroundPositionX: 'center',
    overflow: 'hidden',

    display: 'flex',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'column',
  },
  alertContainer: {
    width: '100%',
    height: 55,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  chipsContainer: {
    display: 'flex',
    justifyContent: 'flex-end',

    width: '97%',
    height: 50,
  },
  mainBottom: {
    width: '100%',
    height: 272,
    backgroundColor: '#fff',

    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  cards: {
    width: '60%',
    height: '100%',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    display: 'flex',
    alignItems: 'center',
  },
  playButton: {
    width: '30%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    '& img': {
      width: '80%',
      cursor: 'pointer',
    },
  },
});

export default useStyles;
