import React, { useState, useEffect } from 'react';
import useStyles from './styles';
import CardAtt from '../../components/cardAtt';
import SteamPlay from '../../assets/images/steam-play-button-not-working.png';
import Alert from '@mui/material/Alert';
import IconButton from '@mui/material/IconButton';
import Collapse from '@mui/material/Collapse';
import CloseIcon from '@mui/icons-material/Close';
import { motion } from 'framer-motion';
import LinearProgress from '@mui/material/LinearProgress';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
//Dialig
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Avatar from '@mui/material/Avatar';
import BackupIcon from '@mui/icons-material/Backup';

const Vt = () => {
  const [ip, setIp] = useState();
  const classes = useStyles();
  let req = new XMLHttpRequest();
  const [open, setOpen] = useState(true);
  const [openDialog, setOpenDialog] = useState(false);

  req.onreadystatechange = () => {
    if (req.readyState == XMLHttpRequest.DONE) {
      /* eslint-disable no-console */
      console.log(JSON.parse(req.responseText).record.rust);
      /* eslint-enable no-console */

      setIp(JSON.parse(req.responseText).record.rust);
    }
  };

  req.open('GET', 'https://api.jsonbin.io/v3/b/63340dc5e13e6063dcb8a482', true);
  req.setRequestHeader(
    'X-Master-Key',
    '$2b$10$puVbArmuZfCopZJOefNKKeVacVmNAfANGxRncr774R3Ltlwf2gYG2',
  );
  req.send();

  const handleClose = () => {
    setOpenDialog(false);
  };

  return (
    <div className={classes.main}>
      <div className={classes.mainTop}>
        <div className={classes.alertContainer}>
          <Collapse in={open}>
            <motion.div
              initial={{ opacity: 0, scale: 0.5 }}
              animate={{ opacity: 1, scale: 1 }}
              transition={{ duration: 0.5, delay: 15 }}
            >
              <Alert
                severity="info"
                variant="standard"
                color="info"
                action={
                  <IconButton
                    aria-label="close"
                    color="inherit"
                    size="small"
                    onClick={() => {
                      setOpen(false);
                    }}
                  >
                    <CloseIcon fontSize="inherit" />
                  </IconButton>
                }
              >
                Evite zerar seu personagem. Faça backup regularmente
              </Alert>
            </motion.div>
          </Collapse>
        </div>
        <div className={classes.chipsContainer}>
          <Stack direction="row" spacing={1}>
            <Chip
              label="V 0.0.4-2"
              href="https://discord.gg/t6w3dwrF"
              color="success"
              icon={<BackupIcon />}
            />
            {/*
            <Chip
              label="Discord"
              href="https://discord.gg/t6w3dwrF"
              onClick={() => {
                ipcRenderer.send('new-window');
              }}
              clickable
              color="success"
              avatar={
                <Avatar
                  alt="Discord"
                  src="https://static.vecteezy.com/ti/vetor-gratis/t2/6892622-discord-logo-icons-editorial-collection-gratis-vetor.jpg"
                />
              }
            />*/}
          </Stack>
        </div>
      </div>
      <div style={{ backgroundColor: '#fff' }}>
        <LinearProgress fullWidth color="success" variant="buffer" />
      </div>

      <div className={classes.mainBottom}>
        <div className={classes.playButton}>
          <a
            onClick={() => setOpenDialog(true)}
            href="steam://run/107410//-connect=143.0.142.127%20-port=2302 -noLauncher -useBE -enableHT -hugepages "
          >
            <img src={SteamPlay} alt="play" />
          </a>
        </div>
        <div className={classes.cards}>
          <CardAtt
            title="Foi feita uma correção no sistema onde o doador perdia o bonus"
            link="#"
            tag="Em Teste"
            data="11/01/23"
          />
          <CardAtt
            title="Foi feita uma Otimização no Cliente, ouve uma melhoria de FPS em todos os clientes"
            link="#"
            tag="Finalizado"
            data="10/01/23"
          />
        </div>
      </div>
      <Dialog
        open={openDialog}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{'VTLauncher'}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Abra seu aplicativo steam para permitir iniciar o jogo
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} autoFocus>
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default Vt;
