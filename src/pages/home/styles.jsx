import { makeStyles } from '@material-ui/styles';
import backgroundImage from '../../assets/images/background.jpg';

const useStyles = makeStyles({
  main: {
    height: '100vh',
    width: '100vw',
    backgroundColor: '#fff',
    overflow: 'hidden',
  },
});

export default useStyles;
