import React, { useState } from 'react';
import useStyles from './styles';

import ConnectImage from '../../assets/images/connect.png';
import LogoSever from '../../assets/images/logoServer.png';

const Home = () => {
  const [ip, setIp] = useState();
  const classes = useStyles();
  let req = new XMLHttpRequest();

  req.onreadystatechange = () => {
    if (req.readyState == XMLHttpRequest.DONE) {
      /* eslint-disable no-console */
      console.log(JSON.parse(req.responseText).record.rust);
      /* eslint-enable no-console */

      setIp(JSON.parse(req.responseText).record.rust);
    }
  };

  let d = new Date();
  let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
  let mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
  let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);

  let dW = d.setDate(d.getDate() + 14);
  let yeW = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(dW);
  let moW = new Intl.DateTimeFormat('en', { month: 'short' }).format(dW);
  let daW = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(dW);

  req.open('GET', 'https://api.jsonbin.io/v3/b/63340dc5e13e6063dcb8a482', true);
  req.setRequestHeader(
    'X-Master-Key',
    '$2b$10$puVbArmuZfCopZJOefNKKeVacVmNAfANGxRncr774R3Ltlwf2gYG2',
  );
  req.send();

  return (
    <div className={classes.main}>
      <div className={classes.mainTop}>
        <img src={LogoSever} alt="connect" />
      </div>
      <div className={classes.mainBottom}>
        <a href={`steam://connect/${ip}`} className={classes.mainBottomContent}>
          <img src={ConnectImage} alt="connect" />
        </a>
        <div className={classes.smallButtons}>
          <div className={classes.buttonData}>
            {`${da} ${mo.toUpperCase()} ${ye}`}
          </div>
          <div className={classes.buttonStatus}>ACTIVE</div>
          <div className={classes.buttonWipe}>
            {/* WIPE : {`${daW} ${moW.toUpperCase()} ${yeW}`} */}
            WIPE : 13 OCT 2022
          </div>
          <div className={classes.tagsContainer}>
            <div className={classes.buttonTag}>PvE</div>
            <div className={classes.buttonTag}>Vanilla</div>
            <div className={classes.buttonTag}>Biweekly</div>
            <div className={classes.buttonTag}>5x Loot</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
