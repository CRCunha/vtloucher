const { app, BrowserWindow, Tray, Menu, ipcMain, shell } = require('electron');
const path = require('path');
let mainWindow;

function createWindow() {
  const win = new BrowserWindow({
    quitOnClose: true,
    appIcon: path.join(__dirname, '/src/assets/images/vt.ico'),
    width: 1280,
    height: 839,
    transparent: true,
    resizable: false,
    autoHideMenuBar: true,
    center: true,

    titleBarStyle: 'hidden',
    title: 'VTLauncher',
    titleBarOverlay: {
      color: '#fff',
      symbolColor: '#78C664',
      height: 30,
    },
    customButtonsOnHover: false,
    icon: __dirname + '/src/assets/images/vt.ico',
    webPreferences: {
      nodeIntegration: true,
      preload: path.join(__dirname, 'preloader.js'),
    },
  });
  //win.loadURL('http://localhost:3000');
  win.loadURL('http://cristianapp.com.br.s3-website-sa-east-1.amazonaws.com');

  win.loadURL(url).then(() => console.log('URL loaded.'));

  let tray = null;
  win.on('minimize', function (event) {
    event.preventDefault();
    win.setSkipTaskbar(true);
    tray = createTray();
  });

  win.on('restore', function (event) {
    win.show();
    win.setSkipTaskbar(false);
    tray.destroy();
  });

  return win;
}

function createTray() {
  let appIcon = new Tray(path.join(__dirname, 'cloud_fun.ico'));
  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Show',
      click: function () {
        mainWindow.show();
      },
    },
    {
      label: 'Exit',
      click: function () {
        app.isQuiting = true;
        app.quit();
      },
    },
  ]);

  appIcon.on('double-click', function (event) {
    mainWindow.show();
  });
  appIcon.setToolTip('Tray Tutorial');
  appIcon.setContextMenu(contextMenu);
  return appIcon;
}

app.whenReady().then(() => {
  createWindow();

  app.on('active', () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createMainWindow();
  }
});

ipcMain.on('abrir-janela-discord', () => {
  let discordWindow = new BrowserWindow({
    width: 300,
    height: 220,
    webPreferences: {
      nodeIntegration: true,
      preload: path.join(__dirname, 'preloader.js'),
    },
  });
  discordWindow.loadURL(`https://discord.gg/t6w3dwrF`);
});

// This is the actual solution
mainWindow.webContents.on('new-window', function (event) {
  event.preventDefault();
  shell.openExternal('https://discord.gg/t6w3dwrF');
});
